package com.libs.firebase.scud;

import com.google.gson.Gson;

import java.io.FileNotFoundException;
import java.io.FileReader;

public class Setting {

    private String fileName;
    private String databaseURL;
    private boolean doDelete;


    public Setting(){}

    public Setting(String fileName, String databaseURL,boolean doDelete) {
        this.fileName = fileName;
        this.databaseURL = databaseURL;
        this.doDelete=doDelete;
    }

    public static Setting fromJson(String fileName) throws FileNotFoundException {
        return new Gson().fromJson(new FileReader(fileName),Setting.class);
    }


    public String getFileName() {
        return fileName;
    }

    public String getDatabaseURL() {
        return databaseURL;
    }

    public boolean isDoDelete() {
        return doDelete;
    }
}
