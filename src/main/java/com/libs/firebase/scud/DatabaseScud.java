package com.libs.firebase.scud;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.database.*;
import com.libs.firebase.interfacesEvent.*;
import com.libs.firebase.model.*;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Map;

public class DatabaseScud {

    private String rid;
    /**
     * Версия протокола.
     * (меняется только если требуются существенные изменения)
     */
    private static final String PROTOCOL_VERSION = "updateTurniketV2/";
    /**
     * Топик добавления новых турникетов.
     */
    private static final String NEW_TURNSTILE_TOPIC = "newTurniket";
    /**
     * Топик команд тркебующих привелегий администратора (кроме добавления пропусков).
     * (к примеру ребут)
     */
    private static final String ADMIN_TOPIC = "admin";
    /**
     * Топик для добавления удаления и настройки пропусков.
     */
    private static final String PASSES_TOPIC = "passes";
    /**
     * Топик отвечающий за дистанционное открытие/закрытие турникета конкретному пользователю с пропуском.
     */
    private static final String OPEN_FOR_USER_TOPIC = "openforuser";
    private final FirebaseDatabase firebaseDatabase;

    private Setting setting;
    private org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(DatabaseScud.class);


    public DatabaseScud(Setting setting,String rid) throws IOException {
       this.setting=setting;
       this.rid=rid;
        try (FileInputStream serviceAccount =new FileInputStream(setting.getFileName())){

            FirebaseOptions options = FirebaseOptions.builder()
                    .setCredentials(GoogleCredentials.fromStream(serviceAccount))
                    .setDatabaseUrl(setting.getDatabaseURL())
                    .build();

            FirebaseApp.initializeApp(options);


            firebaseDatabase=FirebaseDatabase.getInstance();
        } catch (FileNotFoundException e) {
            throw e;
        } catch (IOException e) {
            throw e;
        }


    }

    /************Дистанционный пропуск по пропускам*************/
    /**********************************************************/

    /**
     * Подписаться на команды пропуска пользователя через КПП.
     * @param openForUserCommandsEventsInterface
     */
    public void subscribeOpenForUser(OpenForUserCommandsEventsInterface openForUserCommandsEventsInterface){

        DatabaseReference databaseReference=firebaseDatabase.getReference(PROTOCOL_VERSION+rid+"/"+ OPEN_FOR_USER_TOPIC);

        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                Object obj= snapshot.getValue();
                if(obj instanceof Map){
                    Map<String,Object> map=(Map<String,Object>)obj;
                    setOpenForUserHandler(map, openForUserCommandsEventsInterface,snapshot);
                }
            }
            @Override
            public void onCancelled(DatabaseError error) {
                logger.info("onCancelled code error: "+error.getCode());
                logger.info("cause: "+error.getDetails());
            }
        });
    }

    private void setOpenForUserHandler(Map<String,Object> objectMap, OpenForUserCommandsEventsInterface openForUserCommandsEventsInterface, DataSnapshot snapshot)
    {
                String qr = (String) objectMap.get("qr");
                String track = (String) objectMap.get("track");
                String direction = (String) objectMap.get("direction");
                String penetrationId = (String) objectMap.get("penetrationId");
                openForUserCommandsEventsInterface.openForUser(qr,track,direction,penetrationId);
                if(setting.isDoDelete())
                    snapshot.getRef().setValueAsync(null);
    }


    /*********************Создание нового КПП******************/
    /**********************************************************/

    /**
     * Подписаться на событие создания нового КПП.
     * @param newTurnstileEventsInterface
     */
    public void subscribeNewTurnstile(NewTurnstileEventsInterface newTurnstileEventsInterface){

        DatabaseReference databaseReference=firebaseDatabase.getReference(NEW_TURNSTILE_TOPIC);

        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {

                if(snapshot.getValue() instanceof HashMap && fieldEqualsKeyMap((Map)snapshot.getValue(),Turnstile.class)) {

                    newTurnstileEventsInterface.get(snapshot.getValue(Turnstile.class));
                    if (setting.isDoDelete())
                        snapshot.getRef().setValueAsync(null);
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                logger.info("onCancelled code error: "+error.getCode());
                logger.info("cause: "+error.getDetails());
            }
        });
    }


    private boolean fieldEqualsKeyMap(Map<String,Object> objectMap,Class cls){
        Field[] fields=cls.getDeclaredFields();

        for (int i = 0; i < fields.length; i++) {
            Field f= fields[i];

            if(f.getModifiers()== Modifier.STATIC) {
                logger.info("continue!!!");
                continue;
            }

            if(!objectMap.containsKey(f.getName())){
                return false;
            }
        }
        return true;
    }



    /***************Прочие команды ****************************/
    /**********************************************************/

    /**
     * Подписаться на команды СКУД имеющие админсчкие привелегии.
     * @param eventsScud
     */
    public void subscribeScudCommands(ScudCommandsEventsInterface eventsScud){

        DatabaseReference databaseReferenceForSettings=firebaseDatabase.getReference(PROTOCOL_VERSION+rid+"/"+ ADMIN_TOPIC);
        /**
         * Подписываемся на обновление настроек малины.
         */
        databaseReferenceForSettings.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                Object obj= snapshot.getValue();

                if(obj instanceof Map){

                    Map<String,Object> map=(Map<String,Object>)obj;
                    updateSettingsHandler(map,eventsScud,snapshot);
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                logger.info("onCancelled code error: "+error.getCode());
                logger.info("cause: "+error.getDetails());
            }
        });
    }

    private void updateSettingsHandler(Map<String,Object> objectMap, ScudCommandsEventsInterface eventsScud, DataSnapshot snapshot){
        String command = (String) objectMap.get("command");

        if("UPDATE_CFG".equals(command)) {
            logger.debug("UPDATE_CFG");
            eventsScud.updateTurnstileParams(objectMap);
            if(setting.isDoDelete())
                snapshot.getRef().setValueAsync(null);
        }
        else if("REBOOT".equals(command) ) {
            logger.debug("REBOOT");
            eventsScud.reboot();
            if (setting.isDoDelete())
                snapshot.getRef().setValueAsync(null);

        }else if("RESET".equals(command)){
            logger.debug("RESET");
            eventsScud.reset();
            if (setting.isDoDelete())
                snapshot.getRef().setValueAsync(null);
        }
        else if("UPDATE_ALL_PASSES".equals(command)){
            logger.debug("UPDATE_ALL_PASSES");
            eventsScud.updateAllPasses();
            if(setting.isDoDelete())
                snapshot.getRef().setValueAsync(null);
        }
        else if("SEND_OPEN_COMMAND".equals(command)){
            logger.debug("SEND_OPEN_COMMAND");
            String track = (String) objectMap.get("track");
            String direction = (String) objectMap.get("direction");
            eventsScud.sendOpenCommand(track, direction);
            if(setting.isDoDelete())
                snapshot.getRef().setValueAsync(null);
        }
    }

    /*Команды связанные с редактированием и работой с пропусками.*/
    /*************************************************************/

    /**
     * Подписаться на команды СКУД имеющие админсчкие привелегии.
     * @param passEditingEventsInterface
     */
    public void subscribePassEditingEvents(PassEditingEventsInterface passEditingEventsInterface){

        DatabaseReference databaseReferenceForPass=firebaseDatabase.getReference(PROTOCOL_VERSION+rid+"/"+ PASSES_TOPIC);
        /**
         * Подписываемся на приход новых пропусков.
         */
        databaseReferenceForPass.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                Object obj= snapshot.getValue();

                if(obj instanceof Map){

                    Map<String,Object> map=(Map<String,Object>)obj;
                    updatePassesHandler(map,passEditingEventsInterface,snapshot);
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                logger.info("onCancelled code error: "+error.getCode());
                logger.info("cause: "+error.getDetails());
            }
        });

    }

    private void updatePassesHandler(Map<String,Object> objectMap, PassEditingEventsInterface passEditingEventsInterface, DataSnapshot snapshot)
    {
        objectMap.forEach((k,v)->{
            if(v instanceof Map){
                Map<String,Object> map=(Map<String,Object>)v;
                String command = (String) map.get("command");
                if("NEW_PASS".equals(command)) {
                    logger.debug("NEW_PASS");
                    passEditingEventsInterface.getNewPassEvent(map);
                    if(setting.isDoDelete())
                        snapshot.getRef().setValueAsync(null);
                }
                else if("UPDATE_PASS".equals(command)){
                    logger.debug("UPDATE_PASS");
                    passEditingEventsInterface.updatePassEvent(map);
                    if(setting.isDoDelete())
                        snapshot.getRef().setValueAsync(null);
                }
                else if("REMOVE_PASS".equals(command)){
                    logger.debug("REMOVE_PASS");
                    passEditingEventsInterface.removePassEvent(map);
                    if(setting.isDoDelete())
                        snapshot.getRef().setValueAsync(null);
                }
                else if("REMOVE_TRACKS".equals(command)){
                    logger.debug("REMOVE_TRACKS");
                    passEditingEventsInterface.removeTracks(map);
                    if(setting.isDoDelete())
                        snapshot.getRef().setValueAsync(null);
                }
                else if("ADD_TRACKS".equals(command)){
                    logger.debug("ADD_TRACKS");
                    passEditingEventsInterface.addTracks(map);
                    if(setting.isDoDelete())
                        snapshot.getRef().setValueAsync(null);
                }
                else if("REFRESH_PASS".equals(command)){
                    logger.debug("REFRESH_PASS");
                    passEditingEventsInterface.refreshPass(map);
                    if(setting.isDoDelete())
                        snapshot.getRef().setValueAsync(null);
                }
            }
        });
    }






   }

