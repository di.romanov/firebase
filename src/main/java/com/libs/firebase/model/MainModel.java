package com.libs.firebase.model;

import com.google.gson.Gson;

public class MainModel {

    protected transient static final Gson GSON= new Gson();


    @Override
    public String toString() {
        return GSON.toJson(this);
    }
}
