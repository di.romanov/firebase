package com.libs.firebase.model;

public class Turnstile extends MainModel{

   private String rid;// (string) id турникета
   private String bcid;// (string) id объекта
   private String hash;// (string) hash турникета
   private String jwt;// (string) токен турникета - наверное при ините лучше его сразу присылать
   private boolean development;// (boolean) какой конфиг firebase использовать. если true - девовский.

   private String         updateTokenTime;// (string;//“x,y”) время обновления токена турникета. x - день недели(0-ежедневно), y - час обновления. Пример;// (“0,2”)  - ежедневно с 2.00 до 3.00
   private boolean noPassRefresh;// (boolean) проверять ли timestamp у пропусков


    public String getRid() {
        return rid;
    }

    public String getBcid() {
        return bcid;
    }

    public String getHash() {
        return hash;
    }

    public String getJwt() {
        return jwt;
    }

    public boolean isDevelopment() {
        return development;
    }

    public String getUpdateTokenTime() {
        return updateTokenTime;
    }

    public boolean isNoPassRefresh() {
        return noPassRefresh;
    }
}
