package com.libs.firebase.interfacesEvent;

public interface OpenForUserCommandsEventsInterface {

    /**
     * Открыть для пользователя.
     * @param qr QR код пропуска на который происходит открытие.
     * @param track имя/номер дороги, которую открываем.
     * @param direction тип открытия.
     * @param penetrationId ID Записи на Back в коллекции отправки типа открытия.
     */
    void openForUser(
            String qr,
            String track,
            String direction,
            String penetrationId
    );

}
