package com.libs.firebase.interfacesEvent

/**
 * Интерфейс работы с пропусками.
 * (единственная команда которой здесь нет обновление всех пропусков.)
 */
interface PassEditingEventsInterface {
    fun updatePassEvent(params: Map<String?, Any?>?)
    fun removePassEvent(params: Map<String?, Any?>?)
    fun getNewPassEvent(params: Map<String?, Any?>?)
    fun removeTracks(params: Map<String?, Any?>?)
    fun addTracks(params: Map<String?, Any?>?)
    fun refreshPass(params: Map<String?, Any?>?)
}