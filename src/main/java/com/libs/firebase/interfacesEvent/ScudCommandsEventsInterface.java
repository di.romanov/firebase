package com.libs.firebase.interfacesEvent;

import java.util.Map;

public interface ScudCommandsEventsInterface {
    void updateTurnstileParams(Map<String,Object>  updateParameters);
    void reboot();
    void reset();
    void updateAllPasses();
    /**
     * Отправить команду на открытие шлака.
     * @param track id дороги на которую нужно подать команду об открытии
     * @param direction тип открытия, для входа, для выхода, и для того и другуго....
     */
    void sendOpenCommand(String track, String direction);


}
