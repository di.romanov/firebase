package com.libs.firebase.test;

import com.libs.firebase.interfacesEvent.ScudCommandsEventsInterface;
import com.libs.firebase.scud.DatabaseScud;
import com.libs.firebase.scud.Setting;

import java.io.IOException;
import java.util.Map;
import java.util.Scanner;

public class Main {

    static class TestEvent implements ScudCommandsEventsInterface {


        //@Override
        public void getNewPass(Map<String,Object>  newPass) {
             System.out.println("New pass: "+newPass);
        }

        @Override
        public void updateTurnstileParams(Map<String,Object> updateParameters) {
            System.out.println("updateTurnstileParams: "+updateParameters);
        }

        @Override
        public void reboot() {
            System.out.println("reboot");
        }

        @Override
        public void reset() {
            System.out.println("reset");
        }

        //@Override
        public void updatePass(Map<String,Object> updatePassEvent) {
            System.out.println("updatePass: "+updatePassEvent);
        }

        //@Override
        public void removePass(Map<String, Object> updatePassEvent) {
            System.out.println("removePass");
        }

        @Override
        public void updateAllPasses() {
            System.out.println("updateAllPasses");
        }

        @Override
        public void sendOpenCommand(String track, String direction) {
            System.out.println("sendOpenCommand"+" roadId: "+ track +", open type: "+ direction);
        }


    }


    public static void main(String...args) throws IOException {

                                                                                 //https://test-b952a-default-rtdb.firebaseio.com/
        Setting setting = new Setting("DimonExperiment.json","https://test-b952a-default-rtdb.firebaseio.com/",false);


        DatabaseScud databaseScud= new DatabaseScud(setting,"5e4e870bad81a530a7f");
        databaseScud.subscribeNewTurnstile((turnstile -> System.out.println(turnstile)));
        databaseScud.subscribeScudCommands(new TestEvent());

        //databaseScud.test();
       new Scanner(System.in).nextLine();

    }

}
